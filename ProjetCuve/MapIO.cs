﻿namespace LibIO
{
	/// <exclude />
	public sealed class MapIO 
	{
        /// <summary>
        /// Adresse de base des sorties ToR sur le système d'acquisition.
        /// </summary>
        public const int BASE_STOR = 0xA010;

        /// <summary>
        /// Nombre de capteurs ToR
        /// </summary>
        public const int NB_CPT_TOR = 2;

        /// <summary>
        /// N° de l'entrée pour le capteur ToR 0.
        /// </summary>
        public const int IN_TOR_0 = 0;

        /// <summary>
        /// N° de l'entrée pour le capteur ToR 1.
        /// </summary>
        public const int IN_TOR_1 = 1;

        /// <summary>
        /// Nombre de sorties ToR
        /// </summary>
        public const int NB_OUT_TOR = 2;

        /// <summary>
        /// N° de la sortie ToR N° 0.
        /// </summary>
        public const int OUT_TOR_0 = 0;

        /// <summary>
        /// N° de la sortie ToR N° 1.
        /// </summary>
        public const int OUT_TOR_1 = 1;

        /// <summary>
        /// Adresse de base des entrées analogiques sur le système d'acquisition.
        /// </summary>
        public const int BASE_EAN = 0xA020;

        /// <summary>
        /// Nombre de capteurs An
        /// </summary>
        public const int NB_CPT_An = 2;

        /// <summary>
        /// L'entrée An n° 0.
        /// </summary>
        public const int IN_AN_0 = 0;

        /// <summary>
        /// L'entrée An n° 1.
        /// </summary>
        public const int IN_AN_1 = 1;
    }
}
