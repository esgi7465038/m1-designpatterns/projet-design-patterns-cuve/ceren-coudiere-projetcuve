﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjetCuve.Factory
{
    internal class AnFactory : AbstractFactoryCapteur
    {
        public override AbstractCapteur CreateCapteurInitial(string name)
        {
            return new An(name, 0);
        }
    }
}
