﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjetCuve.Factory
{
    public abstract class AbstractFactoryCapteur
    {
        public abstract AbstractCapteur CreateCapteurInitial(string name);
    }
}
