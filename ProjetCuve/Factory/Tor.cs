﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProjetCuve.Business;
using ProjetCuve.Factory;
using ProjetCuve.State;

namespace ProjetCuve
{
    public class Tor : AbstractCapteur
    {
        private ITorState Etat { get; set; }

        private bool value { get; set; }
        public Tor(string nom) : base(nom)
        {
            Etat = new CapteurOffState();
        }

        public void Handle(Cuve cuve, bool valeur)
        {
            Etat.Handle(this, cuve, valeur);
        }

        public ITorState GetEtat()
        {
            return Etat;
        }

        public void SetEtat(ITorState etat)
        {
            Etat = etat;
        }  
    }
}
