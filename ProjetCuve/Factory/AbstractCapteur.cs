﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjetCuve.Factory
{
    public abstract class AbstractCapteur
    {
        string Nom { get; set; }

        public AbstractCapteur(string nom)
        {
            Nom = nom;
        }

        public string GetNom()
        {
            return Nom;
        }

    }
}
