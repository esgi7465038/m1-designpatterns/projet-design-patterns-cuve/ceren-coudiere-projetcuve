﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProjetCuve.Business;
using ProjetCuve.Factory;

namespace ProjetCuve.State
{
    public class CapteurBasState : ITorState
    {
        public void Handle(Tor capteur, Cuve cuve, bool valeur)
        {
            if (valeur)
            {
                capteur.SetEtat(this);
                cuve.AllumerPompes();
            } else
            {
                capteur.SetEtat(new CapteurOffState());
            }
        }

        public override string ToString()
        {
            return "Capteur Bas";
        }
    }
}
