﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProjetCuve.Business;
using ProjetCuve.Factory;

namespace ProjetCuve.State
{
    public class CapteurHautState : ITorState
    {
        public void Handle(Tor capteur, Cuve cuve, bool valeur)
        {
            if (valeur)
            {
                capteur.SetEtat(this);
                cuve.EteindrePompes();
            } else
            {
                capteur.SetEtat(new CapteurOffState());
            }
        }

        public override string ToString()
        {
            return "Capteur Haut";
        }
    }
}
