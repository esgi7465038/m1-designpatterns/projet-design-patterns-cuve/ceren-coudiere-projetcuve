﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProjetCuve.Business;
using ProjetCuve.Factory;

namespace ProjetCuve.State
{
    public class CapteurOffState : ITorState
    {
        public void Handle(Tor capteur, Cuve cuve, bool valeur)
        {
            if (valeur && capteur.GetNom().Equals("TorHaut"))
            {
                capteur.SetEtat(new CapteurHautState());
            } else if (valeur && capteur.GetNom().Equals("TorBas"))
            {
                capteur.SetEtat(new CapteurBasState());
            }else
            {
                capteur.SetEtat(this);
            }

        }

        public override string ToString()
        {
            return "Capteur Off";
        }
    }
}
