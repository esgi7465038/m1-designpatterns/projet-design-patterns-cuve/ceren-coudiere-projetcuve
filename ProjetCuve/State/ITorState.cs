﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProjetCuve.Business;
using ProjetCuve.Factory;

namespace ProjetCuve.State
{
    public interface ITorState
    {
        void Handle(Tor capteur, Cuve cuve, bool valeur);
    }
}
