﻿using ProjetCuve.Factory;
using ProjetCuve.State;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjetCuve.Business
{
    public class Cuve
    {
        Tor torBas { get; set; }
        Tor torHaut;
        An anNiveau;
        Pompe pompe;

        public Cuve(Tor torBas, Tor torHaut, An anNiveau, Pompe pompe)
        {
            this.torBas = torBas;
            this.torHaut = torHaut;
            this.anNiveau = anNiveau;
            this.pompe = pompe;
        }

        public void EteindrePompes()
        {
            if (torHaut.GetEtat() is CapteurHautState)
            {
                pompe.estEnMarche = false;
            }
            pompe.estEnMarche = true;
        }

        public void AllumerPompes()
        {
            if (torBas.GetEtat() is CapteurBasState)
            {
                pompe.estEnMarche = true;
            }
            pompe.estEnMarche = false;
        }

        public Tor getTorBas()
        {
            return torBas;
        }

        public Tor getTorHaut()
        {
            return torHaut;
        }
    }
}
