﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProjetCuve.State;

namespace ProjetCuve.Business
{
    public class Pompe
    {
        public bool estEnMarche { get; set; }

        public Pompe(bool estEnMarche)
        {
            this.estEnMarche = estEnMarche;
        }
    }
}
